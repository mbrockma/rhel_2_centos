#!/bin/bash -e

## Notes for consideration:
## - This is the RHEL7 to CentOS 7 conversion process used by CLAS Linux Services
## - This is for use with RHEL 7 boxes only.  Since we are getting close to RHEL 6 EOL we are reinstalling these machines.
## - We strongly recommend testing this script and tweeking it for your own environment.

printf "\n\n****************************************\nDownload Files\n****************************************\n\n"
mkdir /tmp/rhel2centos_transmute
cd /tmp/rhel2centos_transmute
curl -O http://mirrors.uiowa.edu/pub/centos/RPM-GPG-KEY-CentOS-7
curl -O http://mirrors.uiowa.edu/pub/centos/7/os/x86_64/Packages/centos-logos-70.0.6-3.el7.centos.noarch.rpm
curl -O http://mirrors.uiowa.edu/pub/centos/7/os/x86_64/Packages/centos-release-7-7.1908.0.el7.centos.x86_64.rpm
curl -O http://mirrors.uiowa.edu/pub/centos/7/os/x86_64/Packages/yum-3.4.3-163.el7.centos.noarch.rpm
curl -O http://mirrors.uiowa.edu/pub/centos/7/os/x86_64/Packages/yum-plugin-fastestmirror-1.1.31-52.el7.noarch.rpm

printf "\n\n****************************************\nYum Update Problematic Packages\n****************************************\n\n"
yum update -y python-urlgrabber rpm nss nss-tools openldap

printf "\n\n****************************************\nDisable Puppet\n****************************************\n\n"
puppet agent --disable "Migrating rhel to centos" || /bin/true

printf "\n\n****************************************\nUnregister w/ RHS\n****************************************\n\n"
subscription-manager unregister

printf "\n\n****************************************\nRemove Packages and Files\n****************************************\n\n"
yum remove -y subscription-*
yum remove -y rhnlib redhat-support-tool redhat-support-lib-python
rpm -e --nodeps redhat-release-server redhat-logos yum
rm -rf /usr/share/doc/redhat-release /usr/share/redhat-release
rm -rf /var/lib/rhsm /var/cache/yum/x86_64/7Server
rm -f /etc/yum.repos.d/redhat.repo
rm -f /etc/yum.repos.d/wazuh_local.repo

printf "\n\n****************************************\nCopy/Import CentOS 7 GPG Key\n****************************************\n\n"
cp /tmp/rhel2centos_transmute/RPM-GPG-KEY-CentOS-7 /etc/pki/rpm-gpg
rpm --import /tmp/rhel2centos_transmute/RPM-GPG-KEY-CentOS-7

printf "\n\n****************************************\nYum Preperation\n****************************************\n\n"
rpm -Uvh /tmp/rhel2centos_transmute/yum-plugin-fastestmirror-1.1.31-52.el7.noarch.rpm /tmp/rhel2centos_transmute/yum-3.4.3-163.el7.centos.noarch.rpm
rpm -Uvh /tmp/rhel2centos_transmute/centos-release-7-7.1908.0.el7.centos.x86_64.rpm /tmp/rhel2centos_transmute/centos-logos-70.0.6-3.el7.centos.noarch.rpm
yum install -y subscription-manager rhn-check

## Puppet is registering with spacewalk, copying CentOS 7 Keys, and adding repositories
printf "\n\n****************************************\nEnable/Run Puppet\n****************************************\n\n"
puppet agent --enable || /bin/true
puppet agent -t || /bin/true

## Pay attention to the output of `yum reinstall \*`  This will let you know which packages didn't reinstall.  
## If you want to remove all traces of RHEL you will have to deal with the remaining packages by hand.
printf "\n\n****************************************\nUpdate all Packages with Yum\n****************************************\n\n"
yum update -y
printf "\n\n****************************************\nRemove RHS Packages that are no longer needed\n****************************************\n\n"
yum remove python-gofer python-pulp-common  python-gofer-proton
printf "\n\n****************************************\nReinstall all Packages with Yum\n****************************************\n\n"
yum reinstall \* -y
printf "\n\n****************************************\nInstall Core Packages with Yum\n****************************************\n\n"
yum install -y @core
printf "\n\n****************************************\nInstall sharutils with Yum\n****************************************\n\n"
yum install -y sharutils

## We are not reinstalling the kernel here so it will still have the RHEL kernel after the upgrade.
printf "\n\n****************************************\nWe want the boot loader to reflect CentOS 7\n****************************************\n\n"
grub2-mkconfig -o /boot/grub2/grub.cfg


